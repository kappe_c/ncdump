/** The initially empty object where all the functions are added. */
define('nu/extensibleModule',{});

define('nu/ModuleExtender',[],() => 
/**
 * Class to add functions to a module to conveniently create markup language elements dynamically.
 * All created functions take as first argument the parent element that the new element shall be 
 * appended to; after that, optionally, the values for typical element attributes or text content 
 * can be passed.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date September 18, 2017
 */
class {

constructor(module, namespace)
{
	this._M = module;
	this._ns = namespace;
}

//------------------------------------------------------------------------------------------------//

/**
 * Create a function that creates an element of the given type.
 */
tagRaw(tagName)
{
	const ns = this._ns;
	this._M[tagName] = par => par.appendChild(document.createElementNS(ns, tagName));
}

/**
 * Create a function that creates an element of the given type containing the given text as 
 * child node.
 */
tagTxt(tagName)
{
	const ns = this._ns;
	this._M[tagName] = (par, str) => 
	{
		const res = par.appendChild(document.createElementNS(ns, tagName));
		if(str !== undefined)
			res.appendChild(document.createTextNode(str));
		return res;
	}
}

/**
 * Create a function *f* that creates an element of the given type and sets the specified 
 * attribute(s) if given at runtime.
 * @param first The name of the element (tag) *f* shall create.
 * @param rest The attributes that *f* accepts as arguments after the parent element.
 */
tagAttrs() { this['tagAttr' + (arguments.length - 1)](...arguments);}

// The following is hard-coded for maximum performance.

tagAttr1(tagName, aName1)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		return res;
	};
}

tagAttr2(tagName, aName1, aName2)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		return res;
	}
}

tagAttr3(tagName, aName1, aName2, aName3)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		return res;
	}
}

tagAttr4(tagName, aName1, aName2, aName3, aName4)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		return res;
	}
}

tagAttr5(tagName, aName1, aName2, aName3, aName4, aName5)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4, aVal5) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		if(aVal5 !== undefined)
			res.setAttribute(aName5, aVal5);
		return res;
	}
}

tagAttr6(tagName, aName1, aName2, aName3, aName4, aName5, aName6)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4, aVal5, aVal6) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		if(aVal5 !== undefined)
			res.setAttribute(aName5, aVal5);
		if(aVal6 !== undefined)
			res.setAttribute(aName6, aVal6);
		return res;
	}
}

tagAttr7(tagName, aName1, aName2, aName3, aName4, aName5, aName6, aName7)
{
	const ns = this._ns;
	this._M[tagName] = (par, aVal1, aVal2, aVal3, aVal4, aVal5, aVal6, aVal7) => {
		const res = par.appendChild(document.createElementNS(ns, tagName))
		if(aVal1 !== undefined)
			res.setAttribute(aName1, aVal1);
		if(aVal2 !== undefined)
			res.setAttribute(aName2, aVal2);
		if(aVal3 !== undefined)
			res.setAttribute(aName3, aVal3);
		if(aVal4 !== undefined)
			res.setAttribute(aName4, aVal4);
		if(aVal5 !== undefined)
			res.setAttribute(aName5, aVal5);
		if(aVal6 !== undefined)
			res.setAttribute(aName6, aVal6);
		if(aVal7 !== undefined)
			res.setAttribute(aName7, aVal7);
		return res;
	}
}

} );

define('nu/html',['nu/extensibleModule', 'nu/ModuleExtender'], 
/**
 * Add functions to conveniently create HTML elements dynamically.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date July 13, 2017
 */
(extensibleModule, ModuleExtender) => {

const M = extensibleModule;
const me = new ModuleExtender(M, 'http://www.w3.org/1999/xhtml');


[
	// base
	'html', 'head', 'meta', 'title', 'body', 'div', 
	// structuring
	'header', 'footer', 'nav', 'aside', 'main', 'article', 'section', 
	// concerning lines
	'br', 'hr', 'wbr', 
	// lists
	'ul', 'ol', 'li', 
].forEach(me.tagRaw, me);

[
	// inline box
	'span', 
	// text composition
	'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 
	// hard semantics, soft style
	'em', 'strong', 'mark', 'dfn', 'del', 'ins', 'kbd', 
	// hard style, soft semantics
	'i', 'b', 'u', 's', 'sub', 'sup', 
	// code
	'code', 'samp', 'pre', 'var', 
	// quotes
	'blockquote', 'q'
].forEach(me.tagTxt, me);

me.tagAttrs('time', 'datetime');

// table
me.tagAttrs('table', 'sortable');
me.tagTxt('caption');
me.tagRaw('colgroup');
me.tagAttrs('col', 'span');

// table content
me.tagRaw('thead');
me.tagRaw('tbody');
me.tagRaw('tfoot');
me.tagRaw('tr');
me.tagTxt('th');
me.tagTxt('td');

// figures
me.tagRaw('figure');
me.tagRaw('picture');
me.tagAttrs('img', 'src', 'alt');
me.tagRaw('figcaption');

// forms
me.tagRaw('form');
me.tagRaw('fieldset');
me.tagTxt('legend');

// input
M.input = (par, type, value, id) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = type;
	if(value !== undefined) {
		res.value = value;
		res.defaultValue = value;
	}
	if(id !== undefined)
		res.id = id;
	return res;
};
me.tagAttrs('label', 'forId');

me.tagAttrs('textarea', 'id', 'minlength', 'maxlength', 'cols', 'rows');

// select
me.tagAttrs('select', 'id');
me.tagAttrs('optgroup', 'label');
M.option = (par, value, txt) => {
	const res = par.appendChild(document.createElement('option'));
	if(value !== undefined) {
		res.value = value;
		if(txt === undefined)
			res.textContent = value;
	}
	if(txt !== undefined)
		res.textContent = txt;
	return res;
};

// number output
me.tagAttrs('output', 'for', 'name');
me.tagAttrs('meter', 'value', 'min', 'max');
me.tagAttrs('progress', 'value', 'max');

// misc
me.tagAttrs('a', 'href', 'target');
me.tagAttrs('link', 'rel', 'href');
me.tagRaw('address');
me.tagAttrs('abbr', 'title');

// multimedia
me.tagRaw('audio');
me.tagRaw('video');
me.tagAttrs('source', 'src', 'type');
me.tagAttrs('track', 'src', 'srclang');

// dynamic
me.tagRaw('details');
me.tagTxt('summary');
me.tagAttrs('menu', 'type', 'label');
me.tagAttrs('menuitem', 'label', 'icon');


// input types

M.inpCheckbox = (par, id, checked) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = 'checkbox';
	if(id !== undefined)
		res.id = id;
	if(checked === true)
		res.checked = true;
	return res;
};

M.inpFile = (par, id, oninput) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = 'file';
	if(id !== undefined)
		res.id = id;
	if(oninput !== undefined)
		res.oninput = oninput;
	return res;
};

M.inpNumber = (par, id, val, step, min, max) => {
	const res = par.appendChild(document.createElement('input'));
	res.type = 'number';
	if(id !== undefined)
		res.id = id;
	if(val !== undefined) {
		res.value = val;
		res.defaultValue = val;
	}
	if(step !== undefined)
		res.step = step;
	if(min !== undefined)
		res.min = min;
	if(max !== undefined)
		res.max = max;
	return res;
};

return true; // Actually only the side effects of requiring this file are important.

} );

define('nu/style',['nu/extensibleModule'], 
/**
 * This function is called once by requirejs and the result (a function that creates a new style 
 * element) is what a programmer gets when they require this module.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 14, 2017
 */
(extensibleModule) => {

const blockHandler = (styleElt, selector) => ({
	// Map camel case to dash.
	get: (blockRaw, property) => 
	{
		property = property.replace(/[A-Z]/g, m => '-' + m.toLowerCase());
		return blockRaw[property];
	},
	
	// Create new declarations if necessary.
	set: (blockRaw, property, newVal) => 
	{
		// camel case to dash
		property = property.replace(/[A-Z]/g, m => '-' + m.toLowerCase());
		const newDecl = newVal === null ? '' : property + ':' + newVal + ';';
		const oldVal = blockRaw[property];
		if(oldVal !== undefined)
		{
			if(newVal === null)
				delete blockRaw[property];
			if(oldVal === newVal)
				return true;
			else
				blockRaw[property] = newVal;
			// Replace the old value. 
			// Notice that the selector may contain special RexExp characters like ".".
			const sel = selector.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
			const oldBlockStr = styleElt.textContent.match(new RegExp(sel + ' {.*?}\n'))[0];
			const oldDecl = property + ':' + oldVal + ';';
			const newBlockStr = oldBlockStr.replace(oldDecl, newDecl);
			styleElt.textContent = styleElt.textContent.replace(oldBlockStr, newBlockStr);
		}
		else if(newVal !== null) // null here would mean deleting a non-existing property.
		{
			// Add new declaration.
			blockRaw[property] = newVal;
			styleElt.textContent = styleElt.textContent.replace(selector + ' {', selector + ' {' + newDecl);
		}
		return true;
	}
});

const rulesHandler = styleElt => ({
	// Create new declaration blocks if necessary.
	get: (rulesRaw, selector) => {
		if(selector in rulesRaw)
			return rulesRaw[selector];
		styleElt.appendChild(document.createTextNode(selector + ' {}\n'));
		return rulesRaw[selector] = new Proxy({}, blockHandler(styleElt, selector));
	},
	// If an object is given, create a new Proxy like above and apply all the declarations.
	set: (rulesRaw, selector, obj) => {
		if(typeof obj !== 'object') {
			console.error('You cannot define a declaration block by anything but an object');
			return false;
		}
		const block = styleElt.rules[selector]; // utilizing the above `get` method
		for(let property in obj)
			block[property] = obj[property];
		return true;
	}
});

/**
 * Create a new style node under `par`.
 * @return Style element that has an additional `rules` property which is an object like 
 * `{ selector1: { property1: value1, property2: value2 }, selector2: etc }`.
 */
extensibleModule.style = par => {
	const res = par.appendChild(document.createElement('style'));
	res.rules = new Proxy({}, rulesHandler(res));
	return res;
};

return true; // Actually only the side effects of requiring this file are important.

} );

define('nu/ViewBox',[],() =>
/**
 * Class to conveniently get and set (the parameters of) a *viewBox* attribute.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date July 18, 2018
 */
class NuViewBox {

/**
 * @param elem {SVGElement} *svg* element to bind this object to.
 * @param [vb] {Array} Data like [minx, miny, w, h] to set (overwrite) the initial value.
 */
constructor(elem, vb)
{
  this._elem = elem;
  if(vb !== undefined)
    elem.setAttribute('viewBox', vb.join(' '));
}

/** Get the view box as an array like `[minx, miny, w, h]`. */
get data() { return this._elem.getAttribute('viewBox').split(' ').map(str => parseFloat(str)); }

/** Set the view box with an array like `[minx, miny, w, h]`. */
set data(arr) { this._elem.setAttribute('viewBox', arr.join(' ')); }

// This class can be extended to allow setting getting the individual values, or allow more 
// high-level functionality like pan and zoom, or a camera shake, etc.

});

define('nu/svg',['nu/extensibleModule', 'nu/ModuleExtender', 'nu/ViewBox'], 
/**
 * Add functions to conveniently create SVG elements dynamically.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date April 14, 2017
 */
(extensibleModule, ModuleExtender, NuViewBox) => {

const M = extensibleModule;
const ns = 'http://www.w3.org/2000/svg';
const me = new ModuleExtender(M, ns);


/**
 * SVG. 
 * @param {number[]} [vb] *viewBox* attribute, like `[origin_x, origin_y, width, height]`.
 * @param {string} [alignx='mid'] Horizontal view box alignment; 'min', 'mid' or 'max'.
 * @param {string} [aligny='mid'] Vertical view box alignment; 'min', 'mid' or 'max'.
 * @param {string} [ar='meet'] Aspect ratio behavior; 'meet' (preserve and fit), 'slice' 
 * (preserve and crop) or 'none' (distort).
 * @param {string} [background='#ffffff'] *background* style.
 * The `width` and `height` attributes are set to "100%", respectively. You may of course choose to 
 * override this explicitly using `setAttribute` or by setting the respective CSS property.
 */
M.svg = (par, vb, alignx, aligny, ar='meet', background='#ffffff') => 
{
	const res = par.appendChild(document.createElementNS(ns, 'svg'));
	// The following line seems redundant but is actually important for export (saving the SVG).
	res.setAttribute('xmlns', ns);
	res.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
	res.setAttribute('width', '100%');
	res.setAttribute('height', '100%');
	res.style.background = background;
	if(vb !== undefined) {
		res.setAttribute('viewBox', vb.join(' '));
		switch(alignx) {
			case 'min': alignx = 'xMin'; break;
			case 'max': alignx = 'xMax'; break;
			default: alignx = 'xMid';
		}
		switch(aligny) {
			case 'min': aligny = 'YMin'; break;
			case 'max': aligny = 'YMax'; break;
			default: aligny = 'YMid';
		}
		res.setAttribute('preserveAspectRatio', alignx + aligny + ' ' + ar);
	}
	return res;
};

/* Title (tooltip) */
me.tagTxt('title');

/* Group. */
me.tagAttrs('g', 'transform');

/* Definitions. */
me.tagRaw('defs');

/* Linear gradient. */
me.tagAttrs('linearGradient', 'id', 'x1', 'y1', 'x2', 'y2', 'gradientUnits', 'gradientTransform');

/* Sample point of a gradient. */
me.tagAttrs('stop', 'offset', 'stop-color', 'stop-opacity');

/* Clip path. */
me.tagAttrs('clipPath', 'id');

/* Line. */
me.tagAttrs('line', 'x1', 'y1', 'x2', 'y2');

/* Polyline. */
me.tagAttrs('polyline', 'points');

/* Polygon. */
me.tagAttrs('polygon', 'points');

/* Rectangle. */
me.tagAttrs('rect', 'x', 'y', 'width', 'height', 'rx', 'ry');

/* Ellipse. */
me.tagAttrs('ellipse', 'cx', 'cy', 'rx', 'ry');

/* Circle. */
me.tagAttrs('circle', 'cx', 'cy', 'r');


/* Text. */
me.tagAttrs('text', 'x', 'y', 'dx', 'dy');
me.tagAttrs('tspan', 'x', 'y', 'dx', 'dy');


/* Path. */
me.tagAttrs('path', 'd');


/**
 * Get a `NuViewBox` object to conveniently set and get the *viewBox* attribute.
 * @param elem {SVGElement} *svg* element to bind the `NuViewBox` object to.
 * @param [vb] {Array} [minx, miny, w, h] to set (overwrite) the initial value.
 */
M.viewBox = (elem, vb) => new NuViewBox(elem, vb);


return true; // Actually only the side effects of requiring this file are important.

} );

define(['nu/extensibleModule', 'nu/html', 'nu/style', 'nu/svg'], 
			 (extensibleModule, html, style, svg) => 
/**
 * Object to export the public interface of the Nu library.
 * @author Christopher Kappe <kappe@cs.uni-kl.de>
 * @date September 19, 2017
 */
{

const M = extensibleModule;

/**
 * Append the string `str` as TextNode to `par`. 
 * @return The new TextNode.
 */
M.txt = (par, str) => par.appendChild(document.createTextNode(str));

// other common stuff
// M.foo;

return M;

} );

