define('netcdf/TypedData',[],() => {

const klass =
/**
 * Class representing named and typed data in NetCDF format, that also has some noteworthy
 * static members.
 */
class NetCdfTypedData {

//------------------------------------------------------------------------------------------------//

/**
 * See the respective getters for a description of the parameters.
 */
constructor(name, typeTag)
{
  this._name = name;
  this._typeTag = typeTag;
}

//------------------------------------------------------------------------------------------------//

/** @return {string} Name of this data. */
get name() { return this._name; }

/** @return {number} Integer in [1,6]. */
get typeTag() { return this._typeTag; }

/** @return {string} One of "Int8", "Uint8", "Int16", "Int32", "Float32", "Float64". */
get type() { return klass.typeTag2str[this._typeTag]; }

//------------------------------------------------------------------------------------------------//

/**
 * @private
 * Swap the bytes of the given `Uint8Array` in chunks of `typeSize`.
 */
static swapBytes(u8Arr, typeSize)
{
  const n = u8Arr.length;
  let tmp;
  if(typeSize === 4) // most likely case first: Float32 or Int32
  {
    for(let i = 0; i < n; i += 2) {
      tmp = u8Arr[i];
      u8Arr[i] = u8Arr[i+3];
      u8Arr[i+3] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[++i];
      u8Arr[i] = tmp;
    }
  }
  else if(typeSize === 8)
  {
    for(let i = 0; i < n; i += 4) {
      tmp = u8Arr[i];
      u8Arr[i] = u8Arr[i+7];
      u8Arr[i+7] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[i+5];
      u8Arr[i+5] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[i+3];
      u8Arr[i+3] = tmp;
      tmp = u8Arr[++i];
      u8Arr[i] = u8Arr[++i];
      u8Arr[i] = tmp;
    }
  }
  else// if(typeSize === 2)
  {
    for(let i = 0; i < n; ++i) {
      tmp = u8Arr[i];
      u8Arr[i] = u8Arr[++i];
      u8Arr[i] = tmp;
    }
  }
}

//------------------------------------------------------------------------------------------------//

/** Convert a string (UTF-16) to an array of UTF-8 character codes. */
static string2utf8Array(str)
{
  const res = [];
  const numCodes = str.length;
  for(let i = 0; i < numCodes; ++i)
  {
    const code16 = str.charCodeAt(i);
    if(code16 < 0x80)
      res.push(code16);
    else if(code16 < 0x800)
      res.push(0xc0 | (code16 >> 6),
               0x80 | (code16 & 0x3f));
    else if(code16 < 0xd800 || code16 >= 0xe000)
      res.push(0xe0 | ( code16 >> 12),
               0x80 | ((code16 >> 6) & 0x3f),
               0x80 | ( code16       & 0x3f));
    else { // surrogate pair
      const code32 = ((code16 & 0x3ff) << 10) | (str.code16At(++i) & 0x3ff)
      res.push(0xf0 |  (code32 >> 18),
               0x80 | ((code32 >> 12) & 0x3f),
               0x80 | ((code32 >>  6) & 0x3f),
               0x80 |  (code32 & 0x3f));
    }
  }
  return res;
}

//------------------------------------------------------------------------------------------------//

/** Convert an array of UTF-8 character codes to a string (UTF-16). */
static utf8Array2string(arr)
{
  let tmp = '';
  for(let code of arr) {
    if(code === 0)
      break;
    tmp += '%' + ('0' + code.toString(16)).slice(-2);
  }
  return decodeURIComponent(tmp);
}

} // END class


// Static data members. We assume that *char* is an *Uint8*.
klass.typeTag2str = [, 'Int8', 'Uint8', 'Int16', 'Int32', 'Float32', 'Float64'];
klass.typeTag2numBytes = [, 1, 1, 2, 4, 4, 8];
klass.typeTag2arrCtor = [, Int8Array, Uint8Array, Int16Array, Int32Array, Float32Array, Float64Array];
klass.str2typeTag = { Int8: 1, Uint8: 2, Int16: 3, Int32: 4, Float32: 5, Float64: 6 };

return klass;

});

define('netcdf/Attribute',['netcdf/TypedData'], (NetCdfTypedData) =>
/**
 * Class representing a NetCDF attribute.
 */
class NetCdfAttribute extends NetCdfTypedData {

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance holding a string.
 */
static fromString(name, string)
{
  return new NetCdfAttribute(name, 2, NetCdfTypedData.string2utf8Array(string), string);
}

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance with the information given in a NetCDF Header.
 * @param {string} [strVal] The data as string.
 */
constructor(name, typeTag, data, strVal)
{
  super(name, typeTag);
  this._data = data;
  if(typeTag === 2)
    this._string = strVal || NetCdfTypedData.utf8Array2string(data);
}

//------------------------------------------------------------------------------------------------//

/** @return {Array|TypedArray} List of values. */
get data() { return this._data; }

/** @return {undefined|string} Concatenation of the characters in case the type is "Uint8". */
get string() { return this._string; }

} );

define('netcdf/Dimension',[],() =>
/**
 * Class representing a NetCDF dimension.
 */
class NetCdfDimension {

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance with the information given in a NetCDF Header.
 */
constructor(name, numValues)
{
  this._name = name;
  this._numValues = numValues;
}

//------------------------------------------------------------------------------------------------//

/** @return {string} The name of this dimension. */
get name() { return this._name; }

/** @return {number} Number of values in this dimension. */
get numValues() { return this._numValues; }

} );

define('netcdf/File',[],() =>
/**
 * Class representing a NetCDF file.
 */
class NetCdfFile {

/**
 * Construct an instance with the information given in a NetCDF Header.
 * The `data` or `records` of the variables need not yet be defined.
 */
constructor(name, numRecords, dims, atts, vars)
{
  this._name = name;
  this._numRecords = numRecords;
  this._dims = dims;
  this._atts = atts;
  this._vars = vars;
  
  this._staticVars = [];
  this._recordVars = [];
  for(let vbl of vars)
   (vbl.isRecordVar !== true ? this._staticVars : this._recordVars).push(vbl);
   
  this.determineVarKind();
}

//------------------------------------------------------------------------------------------------//

/** @return {string} The name of this file (including ".nc"). */
get name() { return this._name; }

/** @return {number|undefined|} Undefined if indeterminate (record dimension is unlimited). */
get numRecords() { return this._numRecords; }

/** @return {Array} List of axes that specify the domain of the variables. */
get dims() { return this._dims; }

/** @return {Array} List of global attributes. */
get atts() { return this._atts; }

/** @return {Array} List of variables. */
get vars() { return this._vars; }


/** @return {Array} Variables (subset of `vars`) that do not depend on the record dimension. */
get staticVars() { return this._staticVars; }

/** @return {Array} Variables (subset of `vars`) that depend on the record dimension. */
get recordVars() { return this._recordVars; }


/**
 * Get an object that maps the kind of a variable (as string) to an array of the respective
 * variables. See `determineVarKind` for details.
 */
get kind2vars() { return this._kind2vars; }

//------------------------------------------------------------------------------------------------//

/**
 * Classify the variables as *data*, *coordinate*, *auxiliary coordinate*, *boundary*, *measure*
 * and *count* as standardized in
 * [CF (Climate and Forecast) Conventions and Metadata](http://cfconventions.org/).
 * See the netcdfjs readme (and or the comments in the code below) for a little more information.
 * @private Typically only called once in constructor.
 */
determineVarKind()
{
  const kind2vars = this._kind2vars =
  {
    data: [],
    coordinate: [],
    auxiliary: [],
    boundary: [],
    measure: [],
    count: []
  };
  
  const classified = new Set();
  
  const add = (vbl, kind) =>
  {
    console.assert(classified.has(vbl) === false, 'Variable "' + vbl.name + '" cannot be classified uniquely (is both "' + vbl.kind + '" and "' + kind + '")');
    kind2vars[kind].push(vbl);
    classified.add(vbl);
    vbl._kind = kind;
  };
  
  const findAndAdd = (vblName, kind) =>
  {
    const vbl = this._vars.find(vbl => vbl.name === vblName);
    console.assert(vbl !== undefined, 'Cannot find variable "' + vblName + '"');
    add(vbl, kind);
  };
  
  const extVar2flag = {};
  {
    const extVars = this._atts.find(a => a.name === 'external_variables');
    if(extVars !== undefined)
      extVars.string.split(' ').forEach(str => extVar2flag[str] = true);
  }
  
  const pairRegExp = /([^ ]+?): ([^ ]+)/g;
  
  // Firstly, classify by explicit attributes.
  for(let vbl of this._vars)
  {
    for(let att of vbl.atts)
    {
      switch(att.name)
      {
        // In the following cases another variable is classified.
        case 'coordinates':
          // The *coordinates* attribute is used on a data variable to unambiguously identify the
          // relevant space and time *auxiliary coordinate* variables.
          // The value of the coordinates attribute is a blank separated list of the names of
          // *auxiliary coordinate* variables.
          att.string.split(' ').forEach(vblName => findAndAdd(vblName, 'auxiliary'));
          break;
        case 'bounds':
          // *coordinate* variables may have a *bounds* attribute whose value is the name of a
          // *boundary* variable.
          findAndAdd(att.string, 'boundary');
          break;
        case 'cell_measures':
          // See cfconventions.org/Data/cf-conventions/cf-conventions-1.7/cf-conventions.html#cell-measures
          // This is a string attribute comprising a list of blank-separated pairs of words of the
          // form "measure: name".
          // For the moment, "area" and "volume" are the only defined measures, but others may be
          // supported in future.
          // The "name" is the name of the variable containing the measure values.
          const cmStr = att.string;
          pairRegExp.lastIndex = 0;
          for(let res = pairRegExp.exec(cmStr); res !== null; res = pairRegExp.exec(cmStr))
          {
            const measure = res[1], mvblName = res[2];
            // The variable is not required to be present in the file. If the variable is located
            // in another file, it must be listed in the *external_variables* attribute of the file.
            const mvbl = this._vars.find(v => v.name === mvblName);
            if(mvbl !== undefined)
              add(mvbl, 'measure');
            else {
              if(extVar2flag[mvblName] !== true)
                console.warn(`The external variable "${mvblName}" is not listed under the global attribute *external_variables*`);
              let fileInfo = '';
              const assoFiles = vbl._atts.find(a => a.name === 'associated_files');
              if(assoFiles !== undefined)
              {
                let base = '', url;
                const afStr = assoFiles.string;
                const idxBak = pairRegExp.lastIndex;
                pairRegExp.lastIndex = 0;
                for(let res = pairRegExp.exec(afStr); res !== null; res = pairRegExp.exec(afStr))
                {
                  const key = res[1], val = res[2];
                  if(key === 'baseURL')
                    base = val;
                  else if(key === mvblName) {
                    url = val;
                    break;
                  }
                }
                pairRegExp.lastIndex = idxBak;
                if(url !== undefined)
                  fileInfo = ` (${base}/${url})`;
              }
              console.info(`The data for the cell measure *${measure}* for the variable "${vbl.name}" is in an external file${fileInfo} as variable "${mvblName}"`);
            }
          }
          break;
        // This classifies the current variable itself.
        case 'sample_dimension':
          // The value of this attribute is the name of the dimension it counts.
          console.assert(this._dims.find(dim => dim.name === att.string) !== undefined, 'The value of the attribute "sample_dimension" (of variable "' + vbl.name + '") must be the name of a dimension but it is "' + att.string + '"');
          // This variable must be of an integer type.
          console.assert(vbl.type.match(/Int(32|16|8)/) !== null, 'The count variable "' + vbl.name + '" must have an integer type but it has "' + vbl.type + '"');
          add(vbl, 'count');
          break;
      }
    }
  }
  
  // Secondly, classify *coordinate* variables by corresponding dimensions, and *data* variables
  // as the rest.
  for(let vbl of this._vars)
  {
    if(classified.has(vbl) === true)
      continue;
    if(this._dims.find(dim => dim.name === vbl.name) !== undefined)
      add(vbl, 'coordinate');
    else
      add(vbl, 'data');
  }
}

} );

define('netcdf/SyntaxError',[],() =>
/**
 * Instances of this class are thrown when there is a syntax error in the NetCDF file being parsed.
 */
class NetCdfSyntaxError extends Error {

constructor(msg)
{
  super(msg);
}

} );

define('netcdf/Variable',['netcdf/TypedData'], (NetCdfTypedData) =>
/**
 * Class representing a NetCDF variable.
 */
class NetCdfVariable extends NetCdfTypedData {

//------------------------------------------------------------------------------------------------//

/**
 * Construct an instance with the information given in a NetCDF Header.
 * @param {number} numValuesPerRecord Total number if not a record variable.
 */
constructor(name = 'scalar_var', typeTag = 6, dimIndices = [], numValuesPerRecord = 1,
            isRecordVar = false, atts = [], datOrRecs)
{
  super(name, typeTag);
  this._dimIndices = dimIndices;
  this._atts = atts;
  this._isRecordVar = isRecordVar;
  this._numValuesPerRecord = numValuesPerRecord;
  if(datOrRecs !== undefined)
    this[isRecordVar === false ? '_data' : '_records'] = datOrRecs;
}

//------------------------------------------------------------------------------------------------//

/** @return {Array} List of dimensions (by zero-based index) that the variable depends on. */
get dimIndices() { return this._dimIndices; }

/** @return {Array} List of attributes. */
get atts() { return this._atts; }

/** @return {boolean} Is it a record variable (one that depends on the record dimension)? */
get isRecordVar() { return this._isRecordVar; }

/**
 * What kind of variable is this?
 * One of "data", "coordinate", "auxiliary", "boundary", "measure", "count".
 * Determined (and further documented) in `NetCdfFile.determineVarKind`.
 */
get kind() { return this._kind; }

/** @return {number} Number of values per record. */
get numValuesPerRecord() { return this._numValuesPerRecord; }

/** @return {number} Number of bytes per record, including padding. */
get numBytesPerRecord()
{
  const n = this._numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[this._typeTag];
  return n + (n % 4);
}

/**
 * @return {Array|TypedArray} Array of values if this is not a record variable; otherwise
 * the records are merged into a single (typed) array (again for each access of the getter).
 */
get data()
{
  if(this._data !== undefined)
    return this._data;
  if(this._kind !== 'data')
    return this._records.reduce((acc, rec) => { acc.push(...rec); return acc; }, []);
  const nPerRec = this._numValuesPerRecord;
  const res = new NetCdfTypedData.typeTag2arrCtor[this._typeTag](nPerRec * this._records.length);
  this._records.forEach((val, idx) => res.set(val, nPerRec * idx));
  return res;
}

/**
 * @return {undefined|Array[Array|TypedArray]} Array of values per record if this is a record 
 * variable. If this is a *data* variable, the values are stored in `TypedArray`s.
 */
get records() { return this._records; }

} );

define('netcdf/Reader',['netcdf/Attribute',
        'netcdf/Dimension',
        'netcdf/File',
        'netcdf/SyntaxError',
        'netcdf/TypedData',
        'netcdf/Variable'],
        (NetCdfAttribute,
         NetCdfDimension,
         NetCdfFile,
         NetCdfSyntaxError,
         NetCdfTypedData,
         NetCdfVariable) =>
/**
 * Class containing methods to read a NetCDF file.
 */
class NetCdfReader {

/**
 * Convenience function to directly read a file in one go.
 * Parameters: see `constructor`. Return value: see `read`.
 */
static read(arrBuf, fileName, wantVerbose)
{
  return (new NetCdfReader(arrBuf, fileName, wantVerbose)).read();
}

//------------------------------------------------------------------------------------------------//

/**
 * Object to read the content of a NetCDF file from an array buffer.
 * @param {ArrayBuffer} arrBuf E.g. as read with `myFileReader.readAsArrayBuffer`.
 * @param {string} fileName E.g. as returned from `myFile.name`.
 * @param {boolean} wantVerbose Log the variable names and static data?
 */
constructor(arrBuf, fileName, wantVerbose = false)
{
  this._ab = arrBuf;
  this._dv = new DataView(arrBuf);
  this._off = 0; // offset pointing to the next unread byte
  this._fileName = fileName;
  this._wantVerbose = wantVerbose;
  
  this._typeTag2readFun =
  [
    undefined,
    () => this._dv.getInt8( this._off++),
    () => this._dv.getUint8(this._off++),
    () => { const res = this._dv.getInt16(this._off); this._off += 2; return res; },
    () => { const res = this._dv.getInt32(this._off); this._off += 4; return res; },
    () => { const res = this._dv.getFloat32(this._off); this._off += 4; return res; },
    () => { const res = this._dv.getFloat64(this._off); this._off += 8; return res; }
  ];
  
  const tagDim = 10, tagVar = 11, tagAtt = 12;
  
  this._listTag2readFun = [];
  this._listTag2readFun[tagDim] = name => this.readDim(name);
  this._listTag2readFun[tagVar] = name => this.readVar(name);
  this._listTag2readFun[tagAtt] = name => this.readAtt(name);
  
  this.readDims = this.getReadListFun(tagDim);
  this.readVars = this.getReadListFun(tagVar);
  this.readAtts = this.getReadListFun(tagAtt);
}

//------------------------------------------------------------------------------------------------//

/**
 * @public
 * @return {NetCdfFile} The completely read file.
 * @throw NetCdfSyntaxError
 */
read()
{
  this.readHeader();
  this.readData();
  return this._file;
}

//------------------------------------------------------------------------------------------------//

/**
 * @public
 * @return {NetCdfFile} The data part is not yet loaded.
 * @throw NetCdfSyntaxError
 */
readHeader()
{
  const cc2str = String.fromCharCode;
  const magic012 =
    cc2str(this._dv.getUint8(this._off++)) +
    cc2str(this._dv.getUint8(this._off++)) +
    cc2str(this._dv.getUint8(this._off++));
  const magic3Raw = this._dv.getUint8(this._off++);
  if(magic012 !== 'CDF')
    throw new NetCdfSyntaxError('Only the NetCDF classic format is supported but we found a "magic number" reading "' + magic012 + cc2str(magic3Raw) + '"');
  if(magic3Raw === 2)
    throw new NetCdfSyntaxError('The 64-bit offset format is not supported');
  if(magic3Raw !== 1)
    throw new NetCdfSyntaxError('Unknown version number "' + magic3Raw + '" (must be "1")');
  
  const numRecsEntry = this.readNonNeg();
  this._numRecords = numRecsEntry === Math.pow(2, 32) - 1 ? undefined : numRecsEntry;
  
  this._dims = this.readDims();
  
  // Get the record dimension (number of values is 0) and make sure that there is at most one.
  this._recordDimIdx = null;
  const numDims = this._dims.length;
  for(let i = 0; i < numDims; ++i) {
    if(this._dims[i].numValues === 0) {
      if(this._recordDimIdx === null)
        this._recordDimIdx = i;
      else
        throw new NetCdfSyntaxError('The number of values in a dimension must be greater than zero, except for the unique record dimension');
    }
  }
  
  const atts = this.readAtts();
  const vars = this.readVars();
  
  return this._file = new NetCdfFile(this._fileName, this._numRecords, this._dims, atts, vars);
}

//------------------------------------------------------------------------------------------------//

getReadListFun(tag)
{
  const read = this._listTag2readFun[tag];
  return () =>
  {
    const listTag = this.readNonNeg();
    if(listTag === 0)
    {
      const zero  = this.readNonNeg();
      this.assert(zero, 0);
      return [];
    }
    this.assert(listTag, tag);
    const n = this.readNonNeg();
    const arr = new Array(n);
    for(let i = 0; i < n; ++i)
      arr[i] = read(this.readName());
    return arr;
  };
}

//------------------------------------------------------------------------------------------------//

readDim(name)
{
  return new NetCdfDimension(name, this.readNonNeg());
}

//------------------------------------------------------------------------------------------------//

readVar(name)
{
  const numDims = this.readNonNeg(); // dimensionality of the domain of this variable
  const dimIndices = new Array(numDims);
  for(let i = 0; i < numDims; ++i)
    dimIndices[i] = this.readNonNeg();
  const atts = this.readAtts();
  const typeTag = this.readTypeTag();
  
  const numBytesPerRecord = this.readNonNeg(); // total number if not a record variable
  const beginByteOffset = this.readNonNeg(); // file seek index
  
  // If this variable depends on the record dimension, the record dimension index must be
  // the first element in `dimIndices`.
  // `dimIndices[0]` may be `undefined`, `this._recordDimIdx` may be `null`.
  const isRecordVar = dimIndices[0] === this._recordDimIdx;
  for(let i = 1; i < numDims; ++i)
    if(dimIndices[i] === this._recordDimIdx)
      throw new NetCdfSyntaxError('The record dimension must be the first one in the list of  dimensions of a variable (it is number ' + (i+1) + ' for the variable "' + name + '")');
  
  // The `numBytesPerRecord` is actually redundant and must match the value one can compute.
  const numBytesPerValue = NetCdfTypedData.typeTag2numBytes[typeTag];
  const numValuesPerRecord = // total number if not a record variable
    dimIndices.map(i => this._dims[i].numValues || 1).reduce((acc, val) => acc * val, 1);
  const computedValue = numBytesPerValue * numValuesPerRecord;
  this.assert(numBytesPerRecord, computedValue + computedValue % 4);
  
  const vbl = new NetCdfVariable(name, typeTag, dimIndices, numValuesPerRecord, isRecordVar, atts);
  // Store the `_beginByteOffset` temporarily for an assertion when the data is read.
  vbl._beginByteOffset = beginByteOffset;
  return vbl;
}

//------------------------------------------------------------------------------------------------//

readAtt(name)
{
  const typeTag = this.readNonNeg();
  if(typeTag === 0 || typeTag > 6)
    throw new NetCdfSyntaxError('Invalid type tag: ' + typeTag);
  const read = this._typeTag2readFun[typeTag];
  const n = this.readNonNeg();
  const data = new Array(n);
  for(let i = 0; i < n; ++i)
    data[i] = read();
  if(typeTag < 4) // potentially padding for byte, char, short
    this.skipPadding();
  return new NetCdfAttribute(name, typeTag, data);
}

//------------------------------------------------------------------------------------------------//

readName()
{
  const n = this.readNonNeg();
  const arr = new Array(n);
  for(let i = 0; i < n; ++i)
    arr[i] = String.fromCharCode(this._dv.getUint8(this._off++));
  this.skipPadding();
  return arr.join('');
}


readNonNeg()
{
  const res = this._dv.getUint32(this._off);
  this._off += 4;
  return res;
}


readTypeTag()
{
  const res = this.readNonNeg();
  if(NetCdfTypedData.typeTag2numBytes[res] === undefined)
    throw new NetCdfSyntaxError('Invalid type tag: ' + res);
  return res;
}


skipPadding()
{
  const remainder = this._off % 4;
  if(remainder !== 0)
    this._off += 4 - remainder;
}


assert(data, hope)
{
  if(data !== hope)
    throw new NetCdfSyntaxError('Expected ' + hope + ', got ' + data);
}

//------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------//

/**
 * @public
 * Read the data part of the file, completing the already existing variables.
 * May only be called after `readHeader`.
 */
readData()
{
  this.readStaticData();
  this.readRecords();
}


/**
 * @public
 * Read the static data part of the file, completing the non-record variables.
 * May only be called after `readHeader`.
 */
readStaticData()
{
  if(this._wantVerbose === true)
    console.log('static variables:');
  for(let vbl of this._file.staticVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    if(vbl !== this._file.staticVars[0] || this._off > vbl._beginByteOffset)
      this.assert(this._off, vbl._beginByteOffset);
    else if(this._off < vbl._beginByteOffset) {
      if(this._wantVerbose === true)
        console.log('There is extra padding between the header and the data:',
                    vbl._beginByteOffset - this._off, 'bytes');
      this._off = vbl._beginByteOffset; // skip the random data
    }
    delete vbl._beginByteOffset;
    const read = this._typeTag2readFun[vbl.typeTag];
    const numValues = vbl._numValuesPerRecord;
    const data = vbl._data = new Array(numValues);
    for(let i = 0; i < numValues; ++i)
      data[i] = read();
    if(this._wantVerbose === true)
      console.log(data);
    if(vbl.typeTag < 4) // potentially padding for byte, char, short
      this.skipPadding();
  }
}


/**
 * @public
 * Read the data records from the file, completing the record variables.
 * May only be called after `readStaticData`.
 */
readRecords()
{
  const numRecs = this._numRecords;
  
  if(numRecs === undefined) { // We currently don't handle streaming data.
    console.error('The number of records in this file is undefined (set to "streaming")');
    return;
  }
  
  const isLittleEndian = new Uint8Array(new Uint32Array([0x12345678]).buffer)[0] === 0x78;
//   console.log('The client processor is', isLittleEndian ? 'little' : 'big', 'endian');
  const swapBytes = NetCdfTypedData.swapBytes;
  const recVars = this._file.recordVars;
  
  const recordSize = recVars.length !== 1 ?
    recVars.reduce((acc, vbl) => acc + vbl.numBytesPerRecord, 0) :
    recVars[0]._numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[recVars[0].typeTag];
  
  // First, define the individual functions to read one record.
  if(this._wantVerbose === true && recVars.length > 0)
    console.log('record variables:');
  for(let vbl of recVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    const records = vbl._records = new Array(numRecs);
    
    const myBeg = vbl._beginByteOffset; delete vbl._beginByteOffset;
    const MyArr = NetCdfTypedData.typeTag2arrCtor[vbl.typeTag];
    const numValuesPerRecord = vbl._numValuesPerRecord;
    const numBytesPerValue = NetCdfTypedData.typeTag2numBytes[vbl.typeTag];
    const numTrueBytesPerRecord = recVars.length !== 1 ? vbl.numBytesPerRecord : recordSize;
    
    const assert = rec => this.assert(this._off, myBeg + recordSize * rec);
    
    if(vbl.kind !== 'data')
    {
      const read = this._typeTag2readFun[vbl.typeTag];
      vbl.readRecord = rec =>
      {
        assert(rec);
        const data = records[rec] = new Array(numValuesPerRecord);
        for(let i = 0; i < numValuesPerRecord; ++i)
          data[i] = read();
        if(vbl.typeTag < 4)
          this.skipPadding();
      };
      continue;
    }
    
    const readBigEndian = rec =>
    {
      assert(rec);
      // NOTE This approach does not work for Float64 data if this._off is not a multiple of 8.
      records[rec] = new MyArr(this._ab, this._off, numValuesPerRecord);
      this._off += numTrueBytesPerRecord;
    };
    vbl.readRecord = (isLittleEndian===false || numBytesPerValue===1) ? readBigEndian : rec =>
    {
      swapBytes(new Uint8Array(this._ab, this._off, numTrueBytesPerRecord), numBytesPerValue);
      readBigEndian(rec);
    };
  }
  
  const recMax = numRecs - 1;
  for(let rec = 0; rec < numRecs; ++rec)
  {
    if(this._wantVerbose === true && rec % 24 === 0)
      console.log('reading records', rec, 'through', Math.min(recMax, rec + 23));
    for(let vbl of recVars)
      vbl.readRecord(rec);
  }
}

} );

define('netcdf/Writer',['netcdf/TypedData'], (NetCdfTypedData) =>
/**
 * Class containing methods to write a NetCDF file.
 */
class NetCdfWriter {

/**
 * Convenience function to directly write a file in one go.
 * Parameters: see `constructor`. Return value: see `write`.
 */
static write(file, wantVerbose)
{
  return (new NetCdfWriter(file, wantVerbose)).write();
}

//------------------------------------------------------------------------------------------------//

/**
 * Object to write the content of a NetCDF file to an array buffer.
 * @param {NetCdfFile} file File to write.
 * @param {boolean} wantVerbose Log the variable names and static data?
 */
constructor(file, wantVerbose = false)
{
  this._ab = null;
  this._dv = null;
  this._off = 0; // offset pointing to the next byte to write
  this._file = file;
  this._wantVerbose = wantVerbose;
  
  this._typeTag2writeFun =
  [
    undefined,
    x => this._dv.setInt8( this._off++, x),
    x => this._dv.setUint8(this._off++, x),
    x => { this._dv.setInt16(  this._off, x); this._off += 2; },
    x => { this._dv.setInt32(  this._off, x); this._off += 4; },
    x => { this._dv.setFloat32(this._off, x); this._off += 4; },
    x => { this._dv.setFloat64(this._off, x); this._off += 8; }
  ];
  
  const tagDim = 10, tagVar = 11, tagAtt = 12;
  
  this._listTag2writeFun = [];
  this._listTag2writeFun[tagDim] = dim => this.writeDim(dim);
  this._listTag2writeFun[tagVar] = vbl => this.writeVar(vbl);
  this._listTag2writeFun[tagAtt] = att => this.writeAtt(att);
  
  this.writeDims = this.getWriteListFun(tagDim);
  this.writeVars = this.getWriteListFun(tagVar);
  this.writeAtts = this.getWriteListFun(tagAtt);
}

//------------------------------------------------------------------------------------------------//

/**
 * @public
 * It is currently only possible to write the file all at once, with this method.
 * @return {ArrayBuffer} The complete file written to an array buffer.
 */
write()
{
  this._ab = new ArrayBuffer(this.compFileSize());
  this._dv = new DataView(this._ab);
  this.writeHeader();
  this.writeData();
  return this._ab;
}

//------------------------------------------------------------------------------------------------//

writeHeader()
{
  this._dv.setUint8(this._off++, 67); // "C"
  this._dv.setUint8(this._off++, 68); // "D"
  this._dv.setUint8(this._off++, 70); // "F"
  this._dv.setUint8(this._off++, 1);
  const numRecs = this._file.numRecords;
  this.writeNonNeg(numRecs !== undefined ? numRecs : Math.pow(2, 32) - 1);
  this.compBeginByteOffset();
  this.writeDims(this._file.dims);
  this.writeAtts(this._file.atts);
  this.writeVars(this._file.vars);
}

compBeginByteOffset() // Is deleted after the data is written.
{
  let nxt = this._beginData;
  for(let vbl of this._file.staticVars) {
    vbl._beginByteOffset = nxt;
    nxt += vbl.numBytesPerRecord;
  }
  // The code may be the same for both kinds of variables but we cannot be sure that the static
  // variables come first in `this._file.vars`.
  for(let vbl of this._file.recordVars) {
    vbl._beginByteOffset = nxt;
    nxt += vbl.numBytesPerRecord;
  }
}

getWriteListFun(listTag)
{
  const write = this._listTag2writeFun[listTag];
  return arr =>
  {
    if(arr.length === 0)
    {
      this.writeNonNeg(0);
      this.writeNonNeg(0);
      return;
    }
    this.writeNonNeg(listTag);
    this.writeNonNeg(arr.length);
    for(let obj of arr)
    {
      this.writeNonNeg(obj._utf8arr.length);
      obj._utf8arr.forEach(x => this._dv.setUint8(this._off++, x));
      delete obj._utf8arr;
      this.addPadding();
      write(obj);
    }
  }
}

writeDim(dim)
{
  this.writeNonNeg(dim.numValues);
}

writeVar(vbl)
{
  this.writeNonNeg(vbl.dimIndices.length);
  vbl.dimIndices.forEach(this.writeNonNeg, this);
  this.writeAtts(vbl.atts);
  this.writeNonNeg(vbl.typeTag);
  this.writeNonNeg(vbl.numBytesPerRecord);
  this.writeNonNeg(vbl._beginByteOffset);
}

writeAtt(att)
{
  const typeTag = att.typeTag;
  this.writeNonNeg(typeTag);
  const numValues = att.data.length;
  this.writeNonNeg(numValues);
  att.data.forEach(this._typeTag2writeFun[typeTag]);
  this.addPadding();
}

writeNonNeg(x)
{
//   console.assert(x >= 0, 'Expected a non-negative integer, got ' + x);
  this._dv.setUint32(this._off, x);
  this._off += 4;
}

addPadding()
{
  const remainder = this._off % 4;
  // JS array buffers are initialized to 0, so we honor the specification for the header padding.
  if(remainder !== 0)
    this._off += 4 - remainder;
}

//------------------------------------------------------------------------------------------------//

// According to the specification, any padding should be filled with the fill value of the
// respective variable. We do not do this currently because this memory is ignored anyway.
/**
 * Write the data part of the file. May only be called after `writeHeader`.
 */
writeData()
{
  this.writeStaticData();
  this.writeRecords();
}


/**
 * Write the static data. May only be called after `writeHeader`.
 */
writeStaticData()
{
  if(this._wantVerbose === true)
    console.log('static variables:');
  for(let vbl of this._file.staticVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    console.assert(this._off === vbl._beginByteOffset,
                   'writer offset: ' + this._off + '  variable offset: ' + vbl._beginByteOffset);
    delete vbl._beginByteOffset;
    vbl.data.forEach(this._typeTag2writeFun[vbl.typeTag]);
    this.addPadding();
  }
}


/**
 * Write the data records. May only be called after `writeStaticData`.
 */
writeRecords()
{
  const numRecs = this._file.numRecords;
  
  if(numRecs === undefined) { // We currently don't handle streaming data.
    console.error('The number of records in this file is undefined (set to "streaming")');
    return;
  }
  
  const isLittleEndian = new Uint8Array(new Uint32Array([0x12345678]).buffer)[0] === 0x78;
//   console.log('The client processor is', isLittleEndian ? 'little' : 'big', 'endian');
  const swapBytes = NetCdfTypedData.swapBytes;
  const recVars = this._file.recordVars;
  
  const recordSize = recVars.length !== 1 ?
    recVars.reduce((acc, vbl) => acc + vbl.numBytesPerRecord, 0) :
    recVars[0]._numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[recVars[0].typeTag];
  
  // First, define the individual functions to write one record.
  if(this._wantVerbose === true && recVars.length > 0)
    console.log('record variables:');
  for(let vbl of recVars)
  {
    if(this._wantVerbose === true)
      console.log(vbl.name);
    const records = vbl._records;
    
    const myBeg = vbl._beginByteOffset; delete vbl._beginByteOffset;
    const MyArr = NetCdfTypedData.typeTag2arrCtor[vbl.typeTag];
    const numValuesPerRecord = vbl._numValuesPerRecord;
    const numBytesPerValue = NetCdfTypedData.typeTag2numBytes[vbl.typeTag];
    const numTrueBytesPerRecord = recVars.length !== 1 ? vbl.numBytesPerRecord : recordSize;
    const assert = rec => console.assert(
      this._off === myBeg + recordSize * rec,
      'writer offset: ' + this._off + '  ' +
      'computed offset: ' + (myBeg + recordSize * rec));
    
    if(vbl.kind !== 'data')
    {
      const write = this._typeTag2writeFun[vbl.typeTag];
      vbl.writeRecord = rec =>
      {
        assert(rec);
        records[rec].forEach(write);
        this.addPadding();
      };
      continue;
    }
    
    const writeBigEndian = rec =>
    {
      assert(rec);
      // NOTE This approach does not work for Float64 data if this._off is not a multiple of 8.
      // temporary variable for readability
      const arrViewOnBuf = new MyArr(this._ab, this._off, numValuesPerRecord);
      arrViewOnBuf.set(records[rec]);
      this._off += numTrueBytesPerRecord; // padding with zeros
    };
    vbl.writeRecord = (isLittleEndian===false || numBytesPerValue===1) ? writeBigEndian : rec =>
    {
      const off = this._off;
      writeBigEndian(rec);
      swapBytes(new Uint8Array(this._ab, off, numTrueBytesPerRecord), numBytesPerValue);
    };
  }
    
  for(let rec = 0; rec < numRecs; ++rec)
  {
    if(this._wantVerbose === true)
      console.log('record', rec);
    for(let vbl of recVars)
      vbl.writeRecord(rec);
  }
}

//------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------------//

compFileSize()
{
  this._beginData = 4 + 4 + // magic + numRecords
    this.compDimListSize(this._file.dims) +
    this.compAttListSize(this._file.atts) +
    this.compVarListSize(this._file.vars);
  return this._beginData + this.compDataSize(this._file.vars);
}

compDimListSize(dims)
{
  let res = 8;
  for(let dim of dims)
    res += this.compNameSize(dim) + 4; // name + numValues
  return res;
}

compAttListSize(atts)
{
  let res = 8;
  for(let att of atts) {
    const numDataBytes = att.data.length * NetCdfTypedData.typeTag2numBytes[att.typeTag];
    const remainder = numDataBytes % 4;
    const padding = remainder === 0 ? 0 : 4 - remainder;
    // name + typeTag + numValues + data
    res += this.compNameSize(att) + 4 + 4 + numDataBytes + padding;
  }
  return res;
}

compVarListSize(vars)
{
  let res = 8;
  for(let vbl of vars)
    // name + numDims + dimIndices + atts + typeTag + numBytesPerRecord + beginByteOffset
    res += this.compNameSize(vbl) + 4 + 4*vbl.dimIndices.length +
           this.compAttListSize(vbl.atts) + 4*3;
  return res;
}

compNameSize(obj)
{
  obj._utf8arr = NetCdfTypedData.string2utf8Array(obj.name);
  let res = obj._utf8arr.length + 4; // name + number of UTF-8 codes
  const remainder = res % 4;
  if(remainder !== 0)
    res += 4 - remainder;
  return res;
}

compDataSize(vars)
{
  let res = 0;
  for(let vbl of this._file.staticVars)
   res += vbl.numBytesPerRecord;
  
  const recVars = this._file.recordVars;
  const numRecs = this._file.numRecords;
  if(recVars.length !== 1)
    return recVars.reduce((acc, vbl) => acc + numRecs * vbl.numBytesPerRecord, res);
  
  const vbl = recVars[0];
  return res + numRecs * vbl.numValuesPerRecord * NetCdfTypedData.typeTag2numBytes[vbl.typeTag];
}

} );

define(['netcdf/Attribute',
        'netcdf/Dimension',
        'netcdf/File',
        'netcdf/Reader',
        'netcdf/SyntaxError',
        'netcdf/TypedData',
        'netcdf/Variable',
        'netcdf/Writer'],
        (NetCdfAttribute,
         NetCdfDimension,
         NetCdfFile,
         NetCdfReader,
         NetCdfSyntaxError,
         NetCdfTypedData,
         NetCdfVariable,
         NetCdfWriter) =>
/** Object to export the public components of the library. */
({

Reader: NetCdfReader,
SyntaxError: NetCdfSyntaxError,
File: NetCdfFile,
Dimension: NetCdfDimension,
Attribute: NetCdfAttribute,
Variable: NetCdfVariable,
TypedData: NetCdfTypedData,
Writer: NetCdfWriter

}) );

