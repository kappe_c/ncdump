# ncdump online

This little web app is inspired by the command line tool ncdump.
It provides similar functionality as the commands `ncdump -h file.nc` (to view the header)
and `ncdump -c file.nc` (to view information about the coordinates).
But while the output of ncdump on the command line can be quite messy, especially for files with
a lot of meta data, ncdump online generates a nice HTML document where the information is assembled
in tables. Besides this eye candy, the (meta) data is shown unaltered as it appears in the file;
the only "derived" information that is included is the classification of dimensions and variables.

This app is based on [netcdfjs](https://bitbucket.org/kappe_c/netcdfjs) and therefore inherits its
limitations, most noteworthy its restriction to the netCDF classic format.

The source code is published under the GPLv3 license.
An [instance of the app](https://www-user.rhrk.uni-kl.de/~kappe/ncdump) is currently hosted by the
TU Kaiserslautern.
