define(['ext/netcdf', 'ext/nu'], (netcdf, nu) =>
/**
 * An instance of this class makes up the application.
 * It is intended to be loaded from *index.html* via *main.js*.
 * @author Christopher Kappe <christopher.p.kappe@ieee.org>
 */
class App {

/**
 * Create a file input bound to the dump functionality.
 * @param {Element} par Parent element in which the GUI is created.
 */
constructor(par)
{
  const inpBox = nu.div(par);
  const lbl = nu.label(inpBox, 'fileInput');
  lbl.textContent = 'NetCDF file: ';
  lbl.title = 'You can drag & drop a file onto the button.';
  const fileInp = nu.inpFile(inpBox, 'fileInput', () =>
  {
    const file = fileInp.files[0];
    const reader = new FileReader();
    reader.onload = () => this.onArrBufRead(reader.result, file.name);
    reader.readAsArrayBuffer(file);
  });
  this._outBox = nu.div(par);
}

//------------------------------------------------------------------------------------------------//

/**
 * A NetCDF file is read and the content dumped into a container element as tables.
 * @param {ArrayBuffer} arrBuf Content of a NetCDF file.
 * @param {string} fileName Name of the respective file.
 */
onArrBufRead(arrBuf, fileName)
{
  const ncReader = new netcdf.Reader(arrBuf, fileName, true);
  let ncFile;
  try { ncFile = ncReader.read(); }
  catch(e) {
    if(e instanceof netcdf.SyntaxError) {
      console.error(e.message);
      alert(e.message);
      return;
    }
    else throw e;
  }
  
  const box = this._outBox;
  while(box.lastChild !== null)
    box.removeChild(box.lastChild);
  
  this.makeBoxAtts(box, ncFile);
  this.makeBoxDims(box, ncFile);
  this.makeBoxVars(box, ncFile);
}

//------------------------------------------------------------------------------------------------//

makeBoxDims(par, ncFile)
{
  const box = nu.div(par);
  if(ncFile.dims.length === 0) {
    nu.h3(box, 'Dimensions');
    nu.p(box, 'None.');
    return;
  }
  
  const dimsConcrete = [];
  const dimsAbstract = [];
  for(let dim of ncFile.dims)
    (ncFile.vars.find(vbl => vbl.name === dim.name) ? dimsConcrete : dimsAbstract).push(dim);
  
  const makeTbl = (par, heading, dims, title) =>
  {
    const h = nu.h3(par, heading);
    if(title !== undefined)
      h.title = title;
    const tbl = nu.table(par);
    const hdrs = nu.tr(tbl);
    nu.th(hdrs, 'name');
    nu.th(hdrs, '#values');
    for(let dim of dims)
    {
      const row = nu.tr(tbl);
      nu.td(row, dim.name);
      nu.td(row, (dim.numValues || ncFile.numRecords + ' (record dim.)'));
    }
  };
  
  if(dimsConcrete.length === 0 || dimsAbstract.length === 0) {
    makeTbl(box, 'Dimensions', ncFile.dims);
  } else {
    makeTbl(nu.div(box), 'Concrete Dimensions', dimsConcrete, 'Dimensions that have variables associated with them.');
    makeTbl(nu.div(box), 'Abstract Dimensions', dimsAbstract, 'Dimensions that merely specify the number of values needed to describe a certain concept.');
  }
}

//------------------------------------------------------------------------------------------------//

makeBoxAtts(par, ncFile)
{
  const box = nu.div(par);
  nu.h3(box, 'Global Attributes');
  if(ncFile.atts.length === 0) {
    nu.p(box, 'None.');
    return;
  }
  
  const tbl = nu.table(box);
  // Typically there are just a few long strings that make the table spread the complete width,
  // and the many almost empty cells look bad.
  tbl.style.maxWidth = '45em';
  const hdrs = nu.tr(tbl);
  nu.th(hdrs, 'name');
  nu.th(hdrs, 'value');
  for(let att of ncFile.atts)
  {
    const row = nu.tr(tbl);
    nu.td(row, att.name);
    // (We need a div to allow for overflow (scroll bars).)
    nu.txt(nu.div(nu.td(row)), att.string || att.data.join(', '));
  }
}

//------------------------------------------------------------------------------------------------//

makeBoxVars(par, ncFile)
{
  const box = nu.div(par);
  if(ncFile.vars.length === 0) {
    nu.h3(box, 'Variables');
    nu.p(box, 'None.');
    return;
  }
  
  const kind2vars = ncFile.kind2vars;
  
  let numKinds = 0;
  for(let kind in kind2vars)
    if(kind2vars[kind].length !== 0)
      ++numKinds;
  
  if(numKinds === 1) {
    nu.h3(box, 'Variables');
    this.makeTblVars(box, ncFile.vars, ncFile);
    return;
  }
  
  const makeKind = kind =>
  {
    const nv = kind2vars[kind].length;
    if(nv === 0)
      return;
    
    const heading = kind === 'auxiliary' ? 'Auxiliary Coordinate Variables' :
      kind.charAt(0).toUpperCase() + kind.slice(1) + ' Variables';
    
    const boxCls = nu.div(box);
    nu.h3(boxCls, heading);
    this.makeTblVars(boxCls, kind2vars[kind], ncFile);
  };
  
  makeKind('data');
  makeKind('coordinate');
  makeKind('auxiliary');
  makeKind('boundary');
  makeKind('measure');
  makeKind('count');
}

//------------------------------------------------------------------------------------------------//

makeTblVars(par, vars, ncFile)
{
  const nv = vars.length;
  if(nv === 0)
    return;
  
  // Collect all attribute names of all variables as column headers 
  // and store the respective key-value pairs in an object for each variable for quick access later.
  const attNamesSet = new Set();
  const v2aName2aVal = new Array(nv);
  for(let v = 0; v < nv; ++v)
  {
    const aName2aVal = v2aName2aVal[v] = {};
    for(let att of vars[v].atts)
    {
      attNamesSet.add(att.name);
      if(att.string !== undefined)
        aName2aVal[att.name] = att.string;
      else // Add word break opportunities between the digits of long numbers.
        aName2aVal[att.name] = att.data.join(', ').replace(/\B(?=(\d{3})+(?!\d))/g, '<wbr>');
    }
  }
  const attNames = [];//Array.from(attNamesSet).sort();

  const preOrderedAtts = ['axis', 'units', 'long_name', 'standard_name'];
  for(let attName of preOrderedAtts)
    if(attNamesSet.has(attName))
      attNames.push(attName), attNamesSet.delete(attName);
  
  let haveMissingVal = false;
  if(attNamesSet.has('missing_value'))
    haveMissingVal = true, attNamesSet.delete('missing_value');
  
  let haveFillVal = false;
  if(attNamesSet.has('_FillValue'))
    haveFillVal = true, attNamesSet.delete('_FillValue');
  
  for(let attName of attNamesSet)
    attNames.push(attName);
  if(haveMissingVal === true)
    attNames.push('missing_value');
  if(haveFillVal === true)
    attNames.push('_FillValue');
  
  // Set up the table.
  const tbl = nu.table(par);
  const hdrs = nu.tr(tbl);
  nu.th(hdrs, 'name');
  nu.th(hdrs, 'domain');
  nu.th(hdrs, 'type');
  for(let attName of attNames) {
    const txt = attName.replace(/_.+_/, '…'); // e.g. "least_significant_digit"
    const th = nu.th(hdrs, txt);
    if(txt !== attName)
      th.title = attName;
  }
  const dataTip = 'Click the table cell to open/download a text file containing the respective data.';
  nu.th(hdrs, 'data').title = dataTip;
  
  const tag2str = [, 'i8', 'char', 'i16', 'i32', 'f32', 'f64'];
  
  // TODO In the rare case that the variable type is "Uint8" (characters),
  // `netcdf.TypedData.utf8Array2string` should be called.
  const previewData = vbl =>
  {
    const f = vbl.typeTag < 5 ? x => x : x => x.toPrecision(4);
    const beg = vbl.data[0];
    const n = vbl.data.length;
    if(n === 1)
      return f(beg);
    const lst = vbl.data[n - 1];
    if(n === 2)
      return f(beg) + ', ' + f(lst);
    return f(beg) + ' … ' + f(lst);
  };
  const previewRecs = vbl =>
  {
    const f = vbl.typeTag < 5 ? x => x : x => x.toPrecision(4);
    const beg = vbl.records[0];
    const n = vbl.records.length;
    if(n === 1 && beg.length === 1)
      return f(beg[0]);
    if(n === 1 && beg.length === 2)
      return f(beg[0]) + ', ' + f(beg[1]);
    const lst = vbl.records[n - 1];
    return f(beg[0]) + ' … ' + f(lst[lst.length - 1]);
  };
  
  // Make the rows.
  for(let v = 0; v < nv; ++v)
  {
    const vbl = vars[v];
    const row = nu.tr(tbl);
    nu.td(row, vbl.name);
    nu.td(row, vbl.dimIndices.map(i => ncFile.dims[i].name).join(', '));
    nu.td(row, tag2str[vbl.typeTag]);
    
    const aName2aVal = v2aName2aVal[v];
    for(let aName of attNames)
    {
      const str = aName2aVal[aName];
      if(str === undefined)
        nu.td(row);
      else // (We need a div to allow for overflow (scroll bars).)
        nu.div(nu.td(row)).innerHTML = str;
    }
    
    const td = nu.td(row);
    td.title = dataTip;
    td.onclick = () =>
    {
      const str = (vbl.isRecordVar ? vbl.records.map(rec => rec.join('\n')) : vbl.data).
        join('\n') + '\n';
      this.saveAs(str, ncFile.name.slice(0, ncFile.name.length - 2) + vbl.name + '.txt');
    }
    nu.txt(td, vbl.isRecordVar ? previewRecs(vbl) : previewData(vbl));
  }
}

//------------------------------------------------------------------------------------------------//

/**
 * Offer to open/download the given `string` as a file called `fileName`.
 */
saveAs(string, fileName)
{
  const blob = new Blob([string], { type: 'text/txt;charset=utf-8' });
  const a = nu.a(this._outBox, URL.createObjectURL(blob), '_blank');
  a.style.display = 'none';
  a.download = fileName;
  a.click();
  this._outBox.removeChild(a);
}

});
